package rest;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import domain.Account;
import service.AccountService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 *
 * @author Bram
 */
@Stateless
@Path("accounts")
public class AccountResource {

    @Inject
    AccountService accountService;

    @GET
    @Path("{limit}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllAccounts(@PathParam("limit") int limit) {
        if(limit < 0) limit = 0;

        List<Account> returnedAccounts = accountService.getAllAccounts(limit);

        if(returnedAccounts.size() <= 0){
            return Response.status(Response.Status.NOT_FOUND).entity("No accounts found").build();
        }

        return Response.ok(returnedAccounts, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("username/{username}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAccountByUsername(@PathParam("username") String username){
        if(username == null && username.trim().length() == 0){
            return Response.serverError().entity("Username cannot be empty!").build();
        }

        List<Account> returnedAccounts = accountService.getAccountByUsername(username);
        if(returnedAccounts.size() <= 0){
            return Response.status(Response.Status.NOT_FOUND).entity("domain.Account not found with username: " + username).build();
        }

        return Response.ok(returnedAccounts, MediaType.APPLICATION_JSON).build();
    }
}
