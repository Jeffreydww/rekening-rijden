package dao;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import domain.Account;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import java.util.List;

/**
 *
 * @author Bram
 */

@Stateless
public class AccountDAOImpl implements AccountDAO {

    @PersistenceContext(name = "myPU")
    EntityManager em;

    public AccountDAOImpl() {
    }

    @Override
    public List<Account> getAllAccounts(int limit) throws PersistenceException {
        if (limit == 0) {
            return em.createNamedQuery("Account.findAll").getResultList();
        }
        return em.createNamedQuery("Account.findAll").setMaxResults(limit).getResultList();
    }

    @Override
    public List<Account> getAccountByEmail(String email) throws PersistenceException {
        return em.createNamedQuery("Account.findByEmail").setParameter("email", email).getResultList();
    }

    @Override
    public List<Account> getAccountByUsername(String username) throws PersistenceException {
        return em.createNamedQuery("Account.findByUsername").setParameter("username", username).getResultList();
    }

    @Override
    public void updateAccount(Account user) throws PersistenceException {
        em.merge(user);
    }

    @Override
    public void insertAccount(Account user) throws PersistenceException {
        em.persist(user);
    }

    @Override
    public void deleteAccount(long userId) throws PersistenceException {

    }
}
