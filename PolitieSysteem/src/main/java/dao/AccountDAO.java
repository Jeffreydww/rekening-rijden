package dao;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import domain.Account;

import javax.persistence.PersistenceException;
import java.util.List;

/**
 *
 * @author Bram
 */
public interface AccountDAO {

    List<Account> getAllAccounts(int limit) throws PersistenceException;

    List<Account> getAccountByEmail(String email) throws PersistenceException;

    List<Account> getAccountByUsername(String username) throws PersistenceException;

    void updateAccount(Account user) throws PersistenceException;

    void insertAccount(Account user) throws PersistenceException;

    void deleteAccount(long userId) throws PersistenceException;

    //hoi
}
