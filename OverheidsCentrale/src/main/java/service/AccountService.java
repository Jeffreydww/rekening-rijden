package service;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import dao.AccountDAO;
import domain.Account;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.PersistenceException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bram
 */
@Stateless
public class AccountService {

    @Inject
    AccountDAO accountDao;

    private static final Logger LOGGER = Logger.getLogger(AccountService.class.getName());
    
    public List<Account> getAllAccounts(int limit) {
        try {
            return accountDao.getAllAccounts(limit);
        } catch (PersistenceException pe) {
            LOGGER.log(Level.FINE, "ERROR while performing getAllAccounts operation; {0}", pe.getMessage());
            return null;
        }
    }

    public List<Account> getAccountByEmail(String email) {
        try {
            return accountDao.getAccountByEmail(email);
        } catch (PersistenceException pe) {
            LOGGER.log(Level.FINE, "ERROR while performing getAccountByEmail operation; {0}", pe.getMessage());
            return null;
        }
    }
    
    public List<Account> getAccountByUsername(String username) {
        try {
            return accountDao.getAccountByUsername(username);
        } catch (PersistenceException pe) {
            LOGGER.log(Level.FINE, "ERROR while performing getAccountByUsername operation; {0}", pe.getMessage());
            return null;
        }
    }
    
    public void updateAccount(Account user) {
        try {
            accountDao.updateAccount(user);
        } catch (PersistenceException pe) {
            LOGGER.log(Level.FINE, "ERROR while performing updateAccount operation; {0}", pe.getMessage());
        }
    }
    
    public Account insertAccount(Account user) {
        Account createdAccount = null;

        try {
            accountDao.insertAccount(user);
            if(accountDao.getAccountByEmail(user.getEmail()).size() > 0) {
                createdAccount = accountDao.getAccountByEmail(user.getEmail()).get(0);
            }
        } catch (PersistenceException pe) {
            LOGGER.log(Level.FINE, "ERROR while performing insertAccount operation; {0}", pe.getMessage());
            return null;
        }

        return createdAccount;
    }
    
    public void deleteAccount(long accountID) {
        try {
            accountDao.deleteAccount(accountID);
        } catch (PersistenceException pe) {
            LOGGER.log(Level.FINE, "ERROR while performing deleteAccount operation; {0}", pe.getMessage());
        }
    }
}
